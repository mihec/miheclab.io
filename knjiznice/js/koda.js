$(document).ready(function(){

	var baseUrl = 'https://rest.ehrscape.com/rest/v1';
	var queryUrl = baseUrl + '/query';

	var username = "ois.seminar";
	var password = "ois4fri";

	var ehrTable = [];


	/**
	* Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
	* enolične ID številke za dostop do funkcionalnosti
	* @return enolični identifikator seje za dostop do funkcionalnosti
	*/
	function getSessionId() {
		var response = $.ajax({
			type: "POST",
			url: baseUrl + "/session?username=" + encodeURIComponent(username) +
			"&password=" + encodeURIComponent(password),
			async: false
		});
		return response.responseJSON.sessionId;
	}

	function getSistolicen(st){
		switch(st){
			case 1:
				return Math.round(Math.random()*20 + 130);
				break;
			case 2:
				return Math.round(Math.random()*20 + 100);
				break;
			case 3:
				return Math.round(Math.random()*20 + 60);
				break;

		}
	}

	function getDiastolicen(st){
		switch(st){
			case 1:
				return Math.round(Math.random()*10 + 90);
				break;
			case 2:
				return Math.round(Math.random()*10 + 70);
				break;
			case 3:
				return Math.round(Math.random()*20 + 40);
				break;
		}
	}
	function getVisina(st){
		switch(st){
			case 1:
				return Math.round(Math.random()*10 + 160);
				break;
			case 2:
				return Math.round(Math.random()*5 + 180);
				break;
			case 3:
				return Math.round(Math.random()*5 + 175);
				break;
		}
	}
	function getMasa(st){
		switch(st){
			case 1:
				return Math.round(Math.random()*10 + 65);
				break;
			case 2:
				return Math.round(Math.random()*5 + 75);
				break;
			case 3:
				return Math.round(Math.random()*5 + 50);
				break;
		}
	}
	function getTemperatura(st){
		switch(st){
			case 1:
				return Math.round(Math.random()*2 + 35);
				break;
			case 2:
				return Math.round(Math.random()*1 + 36);
				break;
			case 3:
				return Math.round(Math.random()*2 + 34);
				break;
		}
	}



	/**
	* Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
	* generiranju podatkov je potrebno najprej kreirati novega pacienta z
	* določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
	* shraniti nekaj podatkov o vitalnih znakih.
	* @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
	* @return ehrId generiranega pacienta
	*/
	function generirajPodatke(stPacienta) {
		var ehrId = "";

		let sessionId = getSessionId();

		$.ajax({
			url: baseUrl + "/ehr",
			type: "POST",
			headers: {"Ehr-Session": sessionId},
			async: false,
			success: function(data){
				var partyData;
				ehrId = data.ehrId;
				switch(stPacienta){
					case 1:
						partyData = {
							firstNames: "Marija",
							lastNames: "Penzjonar",
							dateOfBirth: "1952-7-18T19:30",
							partyAdditionalInfo: [{
								key: "ehrId",
								value: ehrId
							}]
						};
						break;
					case 2:
						partyData = {
							firstNames: "Zdravko",
							lastNames: "Gibalnišek",
							dateOfBirth: "1985-7-18T19:30",
							partyAdditionalInfo: [{
								key: "ehrId",
								value: ehrId
							}]
						};
						break;
					case 3:
						partyData = {
							firstNames: "Mirko",
							lastNames: "Zaspanec",
							dateOfBirth: "1989-7-18T19:30",
							partyAdditionalInfo: [{
								key: "ehrId",
								value: ehrId
							}]
						};
						break;
				}

				$.ajax({
					url: baseUrl + "/demographics/party",
					type: 'POST',
					headers: {"Ehr-Session": sessionId},
					contentType: 'application/json',
					data: JSON.stringify(partyData),
					success: function (party) {
						if (party.action == 'CREATE') {
							var queryParams = {
								"ehrId": ehrId,
								templateId: 'Vital Signs',
								format: 'FLAT',
								committer: 'Una Sestra'
							};
							for(var i=0; i<4; i++){
								var dateInMs = Date.now() - Math.round(Math.random()*2.62974383e11);
								var date = new Date(dateInMs);
								var compositionData = {
									"ctx/time": date.toISOString(),
									"ctx/language": "en",
									"ctx/territory": "SI",
									"vital_signs/body_temperature/any_event/temperature|magnitude": getTemperatura(stPacienta),
									"vital_signs/body_temperature/any_event/temperature|unit": "°C",
									"vital_signs/blood_pressure/any_event/systolic": getSistolicen(stPacienta),
									"vital_signs/blood_pressure/any_event/diastolic": getDiastolicen(stPacienta),
									"vital_signs/height_length/any_event/body_height_length": getVisina(stPacienta),
									"vital_signs/body_weight/any_event/body_weight": getMasa(stPacienta)
								};

								$.ajax({
									url: baseUrl + "/composition?" + $.param(queryParams),
									type: 'POST',
									contentType: 'application/json',
									headers: {"Ehr-Session": sessionId},
									data: JSON.stringify(compositionData),
									async: false,
									error: function(error){
										console.log(error);
									}
								});
							}
						}
					},
					error: function(error){
						console.log(error);
					}
				});

			}
		});

		return ehrId;
	}

	function vrniPodatkeZaPrikaz(ehrId, chart, chart2){
		let sessionId = getSessionId();

		var sistolicen;

		$.ajax({
			url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",
			type: "GET",
			headers: {"Ehr-Session": sessionId},
			success: function(dataBlood){
				$.ajax({
					url: baseUrl + "/view/" + ehrId + "/" + "body_temperature",
					type: "GET",
					headers: {"Ehr-Session": sessionId},
					success: function(dataTemp){
						$.ajax({
							url: baseUrl + "/view/" + ehrId + "/" + "height",
							type: "GET",
							headers: {"Ehr-Session": sessionId},
							success: function(dataHeight){
								$.ajax({
									url: baseUrl + "/view/" + ehrId + "/" + "weight",
									type: "GET",
									headers: {"Ehr-Session": sessionId},
									success: function(dataWeight){
										if(dataBlood.length == 0)
											alertNapaka();
										
										sistolicen = dataBlood[0].systolic;
										addData(chart2, "Pacientov tlak", dataBlood[0].systolic);

										for(var i=0; i<dataBlood.length; i++){
											var data = [];
											data.push(dataBlood[i].diastolic/80);
											data.push(dataBlood[i].systolic/120);
											var bmi = dataWeight[i].weight / Math.pow(dataHeight[i].height/100, 2);
											data.push(bmi/20);
											data.push(dataTemp[i].temperature/37);
											var date = new Date(dataBlood[i].time);

											var container = {
												data: data,
												label: date.toLocaleDateString()
											};

											chart.data.datasets.push(container);
										}
											chart.update();
									}
								});
							}
						});
					}
				});
			},
			error: function(error){alertNapaka()}
		});

		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: "GET",
			headers: {"Ehr-Session": sessionId},
			success: function(data){
				$("#imePacient").html("Ime: <b>"+data.party.firstNames+"</b>");
				$("#priimekPacient").html("Priimek: <b>"+data.party.lastNames+"</b>");
				var date = new Date(data.party.dateOfBirth);
				$("#rojstvoPacient").html("Rojstvo: <b>"+date.toLocaleDateString()+"</b>");
			}
		});

		$.getJSON("http://apps.who.int/gho/athena/api/GHO/BP_06.json?filter=COUNTRY:SVN;YEAR:2014;SEX:MLE&callback=?",function(json){
	  		var povprecje = json.fact[0].value.numeric;
	  		addData(chart2, "Slovensko povprečje", povprecje);
		});

	}

	function addData(chart, label, data) {
		chart.data.labels.push(label);
		chart.data.datasets.forEach((dataset) => {
			dataset.data.push(data);
		});
		chart.update();
	}

	$("#generirajPaciente").click(function(){
		$("#generiraniPacienti").html("");
		for (var i = 0; i < 3; i++) {
			ehrTable[i] = generirajPodatke(i+1);
			var pacient = '<li><a class="pacientEHRid" href=#">'+ehrTable[i]+'</a></li>';
			$("#generiraniPacienti").append(pacient);
		}

		$(".pacientEHRid").click(function(){izrisEHR($(this).html())});
	});

	function alertNapaka(){
		$("#alertNapaka").html('<div class="alert alert-danger alert-dismissable">\
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>\
			<strong>Napaka!</strong> Napaka pri prenosu podatkov, prosim preverite vnos in poskusite ponovno.\
			</div>');
	}

	$("#prikazPodatkov").click(function(){
		var ehr = $("#EHRid").val();
		if(ehr != ""){
			izrisEHR(ehr);
		}else{
			alertNapaka();
		}
	});

	var myRadarChartEHR;
	var myBarChartWHO;
	function izrisEHR(ehrId){
		if(myRadarChartEHR != null)myRadarChartEHR.destroy();
		if(myBarChartWHO != null)myBarChartWHO.destroy();

		var ctx = document.getElementById('myChartEHR').getContext('2d');
		var ctx2 = document.getElementById('myChartWHO').getContext('2d');

		var data = {
			labels: ['Diastoličen', 'Sistoličen', 'BMI', 'Temperatura'],
			datasets: [] 
		};

		var data2 = {
			labels: [],
			datasets: [{data: []}]
		};

		myBarChartWHO = new Chart(ctx2, {
			type: 'bar',
			data: data2,
			options: {
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero:true
						}
					}]
				}
			}
		});

		myRadarChartEHR = new Chart(ctx, {
			type: 'radar',
			data: data,
			options: {
				scale: {
					ticks: {
						beginAtZero:true
					}
				}
			}
		});
		vrniPodatkeZaPrikaz(ehrId, myRadarChartEHR, myBarChartWHO);
	}
});
